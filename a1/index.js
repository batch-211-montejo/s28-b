// 1. insertOne method
db.rooms.insertOne({
	name: "single",
	accomodates: 2,
        price: 1000,
        description: "A simple room with all the basic necessities",
        rooms_available: "10",
        isAvailable: "false"
});


// 2. insertMany Method
db.rooms.insertMany([
	{ name: "double",
	accomodates: 3,
        price: 2000,
        description: "A room fit for a small family going on a vacation",
        rooms_available: 5,
        isAvailable: "false" },
	{ accomodates: 4,
        price: 4000,
        description: "A room with a queen sized bed perfect for a simple gateway",
        rooms_available: 15,
        isAvailable: "false" }
]);

// 3. Find Method
db.rooms.find({ name: "double" })


// 4. updateOne Method
db.rooms.updateOne(
        {
            description: "A room with a queen sized bed perfect for a simple gateway"
        },
        {
            $set: {
                rooms_available: 0
            }
        }
    );


// 5.deleteMany method
db.rooms.deleteMany({ rooms_available: 0 });
