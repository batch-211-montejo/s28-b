// CRUD Operations
/*
	-CRUD is a an acronym for: Create, Read, Update, and Delete
	-Create: the create function allows users to create a new record in database
	-Read: the read function is similar to search function. It allows users to search and retrieve specific records
	-Update: The update function is used to modify existing records that are on our database
	-Delete: the delete function allows users, to remove records from a database that is no longer needed
*/

// CREATE: INSERT Documents
/*
	The mongo shell used JavaScript for its syntax
	-MongoDB deals with objects as it's structure for documents.
	-We can ceate documents by providing objects into our methods
	-JS Syntax: object.object.method({method})
*/

// INSERT ONE
/*
	Syntax:
		db.collectionName.insertOne({})
*/
	db.users.insertOne ({
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact: {
			phone: "87654321",
			email: "janedoe@gmail.com"
		}
	});

// INSERT MANY
/*
	Syntax:
	 db.collectioName.insertMany([ {objectA}, {objectB}])
*/

	db.users.insertMany([
			{
				firstName: "Stephen",
				lastName: "Hawking",
				age: 76,
				contact: {
					phone: "87654321",
					email: "stephenhawking@gmail.com"
				},
				course: ["Phython","React", "PHP"]
			},
			{
				firstName: "Neil",
				lastName: "Armstrong",
				age: 82,
				contact: {
					phone: "87654321",
					email: "neilarmstrong@gmail.com"
				},
				course: ["React", "Larevel", "Sass"]
			}
		])



// READ: FIND/RETRIEVE Documents
/*
	-the documentds will be returned based on their order or storage in the collection
*/

// FIND ALL DOCS
/*
	-Syntax:
	db.collectionName.find();
*/

	db.users.find();


//FIND USING SINGLE PARAMETER
/*
	-Syntax:
		db.collectionName.find({ field. value})
*/ 
	db.users.find({ firstName: "Stephen"})

// FIND USING MULTIPLE PARAMETERS
/*
	Syntax:
		db.collectionName.find({ fieldA: valueA, fieldB: valueB})
*/
	db.users.find({ lastName: "Armstrong", age: 82})

// FIND, PRETTY METHOD
/*
	-the "pretty" method allows is to be able to view the documents returned by our terminal in a "pretier" format.
	-Syntax:
		db.collectionName.fine({ field: value}).pretty();
*/
	db.users.find({ lastName: "Armstrong", age: 82}).pretty()


// UPDATE: EDIT a docs

// UPDATE ONE: Updating a single docs
/*
	Syntax:
		db.collectionName.updateOne({criteria}, {$set: {field:value}});
*/
// For our example, let us create a docs that we will then update
// 1. Insert Initial Docs
	db.users.insert({
		firstName: "Test",
		lastName: "Test",
		age: 0,
		contact: {
			phone: "0000000",
			email: "test@gmail.com"
		}
		course: [];
		department: "none"
	})

// 2. Update the docs
	dn.users.updateOne (
			{firstName: "Test"},
			{
				$set: {

				}
			}
		)


// miniactivity
	db.users.updateOne (
	{firstName: "Bill"},
	{
       $set: {
        firstName: "Bill",
		lastName: "gates",
		age: 65,
		contact: {
			phone: "87654321",
			email: "bill@gmail.com"
		},
		course: ["PHP","Laravel"],
		department: "operations",
        status: "active"
		}
	}
);

db.users.find({firstName: "Bill"}).pretty()


// UPDATE MANY: Updating multiple docs
/*
	Syntax:
		db.collectionName.updateMany({criterua}, {$set {field:value}})
*/
	db.users.updateMany(
			{department: "none"},
			{
				$set: {department: "HR"}
			}

		)

// REPLACE ONE
/*
	-replace one replaced the whole docs
	-if updateOne updates specific fields, replaceOne replaces the whole docs
	-if updateOne update parts, replaceOne replaces 
*/

db.users.replaceOne(
		{ firstName: "Bill"},
		{
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "12345678",
				email: "bill@rocketmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations"
		}
	);

// DELETE: DELETING Docs
// For our example, let us create a document that we will delete 
/*
	-it is good to practice soft deletion or archiving of our docs instead of deleting them or removing them from the system

*/
	db.users.insert({
		firstName: "test"
	});

/*
	Syntax:
	db.collectionName.deleteOne({criteria})
*/
	db.users.deleteOne({
		firstName: "test"
	})
	db.users.find({firstName: "test"})

// DELETE MANY: Delete many docs
/*
	Syntax:
		db.collectionName.deleteMany({criteria})
*/
	db.users.deleteMany({
		firstName: "Bill"
	})

	db.users.deleteMany({firstName: "Bill"}).pretty()


// DELETE ALL: Delete all documents
/*
	-Syntax:
		db.collectionName.deleteMany()	
*/


//ADVANCED QUERIES
/*
	-retrieving data with cmoplex data structures is also a good skill for any dveloper to have
	-real world examples of data can be as complex as having two or more layers pf nested objects
	-Learning to query these kinds of data is also essential to end=sure that we are able to retrieve any information that we would need in our application
*/

// Query an embedded document
// -an embedded document are those types of documents that contain a docs inside a docs
	db.users.find({
		contact: {
			phone: "87654321",
			emails: "stephenhawkling@gmail.com"
		}
	}).pretty();


// Query on nested field
	db.users.find({
		"ontact.email": "janedoe@gmail.com"
	}).pretty()


// Quering an Array with Exact Elements
	db.users.find({courses: ["CSS", "JavaScript", "Python"]}).pretty();

// Querying an Array without regard to order
	db.users.find({courses: ["React", "Python"]}).pretty();

// Querying an Embedded Array
	db.users.insertOne({
		namearr: [
		{
			nameA: "Juan"
		},
		{
			nameB: "Tamad"
		}
		]
	})
	db.users.find({
		namearr:{
			nameA: "Juan"
		}
	}).pretty()

